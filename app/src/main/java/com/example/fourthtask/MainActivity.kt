package com.example.fourthtask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.fourthtask.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.hollywood.setOnClickListener {

                binding.adventure.animate().apply {
                    rotationYBy(360f)
                    rotationXBy(360f)
                    duration = 3000
                }.start()
                binding.crime.animate().apply {
                    rotationBy(360f)
                    rotationXBy(360f)
                    duration = 3000
                }.start()
                binding.documentary.animate().apply {
                    rotationBy(360f)
                    rotationXBy(360f)
                    duration = 3000
                }.start()
                binding.drama.animate().apply {
                    rotationBy(360f)
                    rotationXBy(360f)
                    duration = 3000
                }.start()
                binding.detective.animate().apply {
                    rotationBy(360f)
                    rotationXBy(360f)
                    duration = 3000
                }.start()
                binding.noir.animate().apply {
                    rotationBy(360f)
                    rotationXBy(360f)
                    duration = 3000
                }.start()
                binding.western.animate().apply {
                    rotationBy(360f)
                    rotationXBy(360f)
                    duration = 3000
                }.start()
        }
    }
}